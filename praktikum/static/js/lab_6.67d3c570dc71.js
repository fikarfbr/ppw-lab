var print = document.getElementById('print');
var erase = false;
var array = [];
var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];




localStorage.setItem('themes', JSON.stringify(themes));
//localStorage.setItem('selected', JSON.stringify(selected));
// Retrieve the object from storage
var retrievedObject = localStorage.getItem('themes');
var retrievedTheme = localStorage.getItem('newSelected');

//console.log('retrievedObject: ', JSON.parse(retrievedObject));
//console.log('retrievedTheme: ', JSON.parse(retrievedTheme));


var go = function(x) {
  if (x === 'ac') {
    print.value = " ";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

function sendMessage(e) {
    if(e.keyCode == 13){
        var textarea = document.getElementById('chat');
        var msg = document.getElementById('chat').value;
        var old =$(".msg-insert").html();
        array.push(msg);
        //console.log(array)
        for(var i = 0; i < array.length; i++){
            $(".msg-insert").html(old + "<p class = 'msg-send'>" + array[i] + "</p>");
        }

        textarea.value = "";
         $( "#chat" ).attr('placeholder',"press enter");
        return false;
    }
    return true;

}

$(document).ready(function(){
    $("#arrow").click(function() {
        $(".chat-body").slideToggle();
    });
    $('#selector').select2({
        'data' : JSON.parse(retrievedObject)
    });
    var retrievedTheme = localStorage.getItem('newSelected');
    var appliedTheme = JSON.parse(retrievedTheme);
    $('body').css('background', appliedTheme.selected.bcgColor);
    $('body').css('color', appliedTheme.selected.fontColor);
});





$('.apply').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var id = $(".my-select").select2("val");
    //console.log(id);

    data = $.parseJSON(retrievedObject);
    $.each(data, function(i, item) {
    //console.log(item.id)
    if(id == item.id){
        var text = item.text;
        var background = item.bcgColor;
        var font = item.fontColor;

        $('body').css("background-color",background);
        $('body').css("color", font);

        var tes = {"selected":{"bcgColor":background,"fontColor":font, "text":text}};
        //console.log(tes);
        localStorage.setItem('newSelected', JSON.stringify(tes));
        }
    });
//localStorage.setItem('selected', JSON.stringify(selected));
var retrievedTheme = localStorage.getItem('newSelected');

//console.log(appliedTheme);
var appliedTheme = JSON.parse(retrievedTheme);
console.log(appliedTheme);
});


    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada V

    // [TODO] ambil object theme yang dipilih V

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya V

    // [TODO] simpan object theme tadi ke local storage selectedTheme
